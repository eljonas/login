package com.example.login;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.POST;

public interface Api {

    @POST ("register")
    Call<ResponseBody> createUser(
            @Field("name") String nombreUsuario,
            @Field("lastName") String apellidoUsuario,
            @Field("email") String correo,
            @Field("phoneNumber") String celular,
            @Field("password") String contra
    );
}
