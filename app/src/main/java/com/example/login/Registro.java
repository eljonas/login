package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.print.PrinterId;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.login.Utils.Utils;

public class Registro extends AppCompatActivity {

    private EditText nombreUsuario, apellidoUsuario, correo, telefono,contrasena;
    private Button btnRegistro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        nombreUsuario = findViewById(R.id.editTextNombre);
        apellidoUsuario = findViewById(R.id.editTextApellido);
        correo = findViewById(R.id.editTextCorreo);
        telefono = findViewById(R.id.editTextTelefono);
        contrasena = findViewById(R.id.editTextContraseña);
        btnRegistro = findViewById(R.id.btnRegistrar);

        btnRegistro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validaciones ();
            }
        });

    }

    private void Validaciones () {

        String email = correo.getText().toString().trim();


            if (TextUtils.isEmpty(nombreUsuario.getText().toString())) {
                nombreUsuario.setError("Ingrese su nombre.");
                nombreUsuario.requestFocus();

            } else if (TextUtils.isEmpty(apellidoUsuario.getText().toString())) {
                apellidoUsuario.setError("Ingrese sus Apellidos.");
                apellidoUsuario.requestFocus();


            } else if (TextUtils.isEmpty(correo.getText().toString())) {
                correo.setError("Ingrese correo válido.");
                correo.requestFocus();


            } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                correo.setError("Enter");
                correo.requestFocus();


            }
            else if (TextUtils.isEmpty(telefono.getText().toString())) {
                telefono.setError("Ingrese contraseña");
                telefono.requestFocus();


            } else if (TextUtils.isEmpty(contrasena.getText().toString())) {
                contrasena.setError("Valide contraseña");
                contrasena.requestFocus();

            }

        }


}